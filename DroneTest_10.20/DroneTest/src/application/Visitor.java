package application;

abstract class Visitor {

	public abstract void visitItemContainer(ItemContainer itemContainer);
	public abstract void visitItem(Items item);
	
}
