package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class FXMLEditFieldsController implements Initializable {

    @FXML
    private TextField editNameField;

    @FXML
    private TextField editPriceField;

    @FXML
    private TextField editXField;

    @FXML
    private TextField editYField;

    @FXML
    private TextField editLengthField;

    @FXML
    private TextField editWidthField;

    @FXML
    private TextField editHeightField;

    @FXML
    private Button cancelButton;

    public TreeView<String> tree;

    public TreeItem<String> selected_item;

    private Stage dialogStage;

    private boolean okClicked = false;

    public void treePass(TreeView<String> treeView, TreeItem<String> selected) {
        tree = treeView;
        selected_item = selected;
    }

    public void setItem(TreeView<String> treeView) {
        FarmItems item1 = new Items("",0.0,0.0,0.0,0,0,0);

        String tempitem = treeView.getSelectionModel().getSelectedItem().getValue();
//        boolean isItem = FarmItems.getItemData().contains(tempitem);

        for(FarmItems item : FarmItems.getItemContainerData()){
            if(item.getName().equals(tempitem)){
//                System.out.println(item);
                item1 = item;
            }
        }


        editNameField.setText(item1.getName());
        editPriceField.setText(Double.toString(item1.getPrice()));
        editXField.setText(Double.toString(item1.getXLocation()));
        editYField.setText(Double.toString(item1.getYLocation()));
        editLengthField.setText(Integer.toString(item1.getLength()));
        editWidthField.setText(Integer.toString(item1.getWidth()));
        editHeightField.setText(Integer.toString(item1.getHeight()));

    }

    public boolean isOkClicked() {
        return okClicked;
    }


    @FXML
    private void handleOk() {
        // TODO: make it so items cannot be added to a node created with 'Add Item' button

        if (validInput()) {

            FarmItems item1 = new Items();
            item1.setName(editNameField.getText());
            item1.setPrice(Double.parseDouble(editPriceField.getText()));
            item1.setXLocation(Double.parseDouble(editXField.getText()));
            item1.setYLocation(Double.parseDouble(editYField.getText()));
            item1.setLength(Integer.parseInt(editLengthField.getText()));
            item1.setWidth(Integer.parseInt(editWidthField.getText()));
            item1.setHeight(Integer.parseInt(editHeightField.getText()));

            TreeItem<String> treeItem = new TreeItem<>(item1.getName());
            tree.getSelectionModel().getSelectedItem().getChildren().add(treeItem);

            FarmItems.addItem(item1);

            okClicked = true;
            Stage stage = (Stage) cancelButton.getScene().getWindow();
            stage.close();

        }

    }




    @FXML
    public void handleCancel(ActionEvent event) {
        try {
            Stage stage = (Stage) cancelButton.getScene().getWindow();
            stage.close();

        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }


    //TODO add typechecking visitor

    private boolean validInput() {
        String errorMessage = "";

        if (editNameField.getText() == null || editNameField.getText().length() == 0) {
            errorMessage += "Invalid name!\n";
        }


        if (editPriceField.getText() == null || editPriceField.getText().length() == 0) {
            errorMessage += "Invalid price!\n";
        } else {
            // try to parse the postal code into an double.
            try {
                Double.parseDouble(editPriceField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Invalid price (must be a number)!\n";
            }
        }

        if (editXField.getText() == null || editXField.getText().length() == 0) {
            errorMessage += "Invalid X Location!\n";
        } else {

            try {
                Double.parseDouble(editXField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Invalid X Location (must be a number)!\n";
            }
        }


        if (editYField.getText() == null || editYField.getText().length() == 0) {
            errorMessage += "Invalid Y Location!\n";
        } else {

            try {
                Double.parseDouble(editYField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Invalid Y Location (must be a number)!\n";
            }
        }


        if (editLengthField.getText() == null || editLengthField.getText().length() == 0) {
            errorMessage += "Invalid Length!\n";
        } else {

            try {
                Integer.parseInt(editLengthField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Invalid Length (must be an number)!\n";
            }
        }

        if (editWidthField.getText() == null || editWidthField.getText().length() == 0) {
            errorMessage += "Invalid Length!\n";
        } else {

            try {
                Integer.parseInt(editWidthField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Invalid Height (must be an number)!\n";
            }
        }

        if (editHeightField.getText() == null || editHeightField.getText().length() == 0) {
            errorMessage += "Invalid Height!\n";
        } else {

            try {
                Integer.parseInt(editHeightField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Invalid Height (must be an number)!\n";
            }
        }



        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
