package application;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.shape.Rectangle;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.animation.TranslateTransition;
import javafx.util.Duration;


public class FXMLController implements Initializable{
	
	@FXML
	public AnchorPane root;

	@FXML
	public TextField screen;
	
	@FXML
	public ImageView drone;
	
	@FXML
	public Button add_itemContainer;
	
	@FXML
	public Button add_item;

	@FXML
	public Button refresh;

	@FXML
	public TreeView<String> tree = new TreeView<>();

	public TreeItem<String> rootItem = new TreeItem<>("Farm Items");

	@FXML
	private void handleLaunchButton(ActionEvent event) {
//		drone.setCenterX(0);
//		drone.setCenterY(0);
		
		TranslateTransition fly = new TranslateTransition();
		fly.setDuration(Duration.millis(1000));
		fly.setNode(drone);
		fly.setByX(300);
		fly.play();

		System.out.println(FarmItems.getItemContainerData().toString());
		System.out.println(FarmItems.getItemData().toString());



		//NO ANIMATION
//		translate.setX(300);
//		translate.setY(50);
//		translate.setZ(100);
		
//		drone.getTransforms().addAll(translate);
		
		
		
	}
	
	@FXML
	private void handleAddItemContainer(ActionEvent event) {

		try {
			if (tree.getSelectionModel().getSelectedItem() != null) {
				FXMLLoader edititem = new FXMLLoader(getClass().getResource("EditItemContainer.fxml"));
				Parent root1 = edititem.load();

				FXMLEditController editController = edititem.getController();
				editController.treePass(tree);

				Stage dialogStage = new Stage();
				dialogStage.setScene(new Scene(root1));
				dialogStage.show();
			}
			else
				screen.setText("error: please select container to add to");
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	private void handleAddItemButton(ActionEvent event) {
		try {
			if (tree.getSelectionModel().getSelectedItem() != null) {
				FXMLLoader edititem = new FXMLLoader(getClass().getResource("EditItem.fxml"));
				Parent root1 = edititem.load();

				FXMLItemEditController editController = edititem.getController();
				editController.treePass(tree);

				Stage dialogStage = new Stage();
				dialogStage.setScene(new Scene(root1));
				dialogStage.show();
			}
			else
				screen.setText("error: please select container to add to");
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	

	@FXML
	private void handleDeleteButton(ActionEvent event) {
		try {
			if (tree.getSelectionModel().getSelectedItem() != null) {
				TreeItem removeItem = tree.getSelectionModel().getSelectedItem();
				removeItem.getParent().getChildren().remove(removeItem);
				setItem(removeItem);
				screen.setText("Deleted");
			} else
				screen.setText("error: please select item to delete");
		} catch(Exception ex) {
			System.out.println("error: please select item to delete");
		}
	}
	
	
	@FXML
	private void handleEditButton(ActionEvent event) {
		// https://openjfx.io/javadoc/14/javafx.controls/javafx/scene/control/TreeView.EditEvent.html ?
		try {
			if (tree.getSelectionModel().getSelectedItem() != null) {
				FXMLLoader edititem = new FXMLLoader(getClass().getResource("EditFields.fxml"));
				Parent root1 = edititem.load();

				FXMLEditFieldsController editController = edititem.getController();
				TreeItem<String> selected = tree.getSelectionModel().getSelectedItem();
				editController.treePass(tree, selected);
				editController.setItem(tree);

				Stage dialogStage = new Stage();
				dialogStage.setScene(new Scene(root1));
				dialogStage.show();
			}
			else
				screen.setText("error: please select container to edit");
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@FXML
	public void handleRefreshButton(ActionEvent event) {
		clearFarm();

		for (FarmItems item : FarmItems.getItemData()) {
			drawItem(item);
		}

		for (FarmItems container : FarmItems.getItemContainerData()) {
			drawItem(container);
		}
	}

	public void drawItem(FarmItems item) {
		Rectangle rectangle = new Rectangle();
		rectangle.setFill(null);
		rectangle.setStroke(Color.BLUEVIOLET);
		rectangle.setHeight(item.getHeight());
		rectangle.setWidth(item.getWidth());
		rectangle.setX(item.getXLocation());
		rectangle.setX(item.getYLocation());

		Text itemText = new Text(item.getName());

		root.getChildren().add(rectangle);
	}

	public void clearFarm() {
		if (root.getChildren() != null)
			root.getChildren().clear();
	}

	public void setItem(TreeItem<String> treeItem) {
		FarmItems item1 = new Items("", 0.0, 0.0, 0.0, 0, 0, 0);
		FarmItems itemContainer1 = new ItemContainer("", 0.0, 0.0, 0.0, 0, 0, 0);

		String tempitem = treeItem.getValue();
		boolean isItem = FarmItems.getItemData().contains(tempitem);

		if(isItem){
			FarmItems.getItemData().remove(findItem(tempitem, item1));
		} else{
			FarmItems.getItemContainerData().remove(findItemContainer(tempitem, itemContainer1));
		}
	}

	public FarmItems findItemContainer(String tempitem, FarmItems itemContainer1){
		for (FarmItems item_container : FarmItems.getItemContainerData()) {
			if (item_container.getName().equals(tempitem)) {
				itemContainer1 = item_container;
				return itemContainer1;
			}
		}
		return null;
	}

	public FarmItems findItem(String tempitem, FarmItems item1){
		for (FarmItems item : FarmItems.getItemData()) {
			if (item.getName().equals(tempitem)) {
				item1 = item;
				return item1;
			}
		}
		return null;
	}



	@Override
	public void initialize(URL location, ResourceBundle resources) {
		tree.setRoot(rootItem);
//		drawItem(new ItemContainer("poop", 0.00, 500, 500, 300, 300, 300));
	}
}
