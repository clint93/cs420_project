package application;

public class Items extends FarmItems {

	public Items() {
		
	}
	
	
    public Items(String name, double price, double x_coord, double y_coord, int length, int width, int height){
        this.name = name;
        this.price = price;
        this.x_coord = x_coord;
        this.y_coord = y_coord;
        this.length = length;
        this.width = width;
        this.height = height;
    }
	
    public void delete(){
        name = "";
        price = 0;
        x_coord = 0.0;
        y_coord = 0.0;
        length = 0;
        width = 0;
        height = 0;
    }
    
    public void accept(Visitor v) {
    	v.visitItem(this);
    }
    
	
	
}
